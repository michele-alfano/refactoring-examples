package ExtractFunction;

import java.util.ArrayList;
import java.util.Calendar;

public class PrintOwing_before {

    public void printOwing(Invoice invoice) {
        var outstanding = 0;

        // print banner
        System.out.println("************************");
        System.out.println("***** Customer Ows *****");
        System.out.println("************************");

        // calculate outstanding
        for (Order o : invoice.getOrders()) {
            outstanding += o.getAmount();
        }

        // record due date
        Calendar today = Calendar.getInstance();
        Calendar dueDate = today;
        dueDate.add(Calendar.DATE, 30);
        invoice.dueDate = dueDate;

        // print details
        System.out.println("name: " + invoice.getCustomer());
        System.out.println("amount: " + outstanding);
        System.out.println("due: " + invoice.getDueDate().getTime());

    }

}

class Invoice {
    String customer;
    ArrayList<Order> orders;
    Calendar dueDate;

    public Invoice(String customer) {
        this.customer = customer;
        dueDate = Calendar.getInstance();
        this.orders = new ArrayList<>();
    }

    public void addOrder(int amount) {
        Order order = new Order();
        order.setAmount(amount);
        orders.add(order);
    }

    public String getCustomer() {
        return this.customer;
    }

    public ArrayList<Order> getOrders() {
        return this.orders;
    }

    public Calendar getDueDate() {
        return this.dueDate;
    }
}

class Order {
    private static int maxId;
    private String id;
    private int amount;

    public Order() {
        int max = Order.maxId;
        max++;
        Order.maxId = max;
        this.id = Integer.toString(maxId);
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
