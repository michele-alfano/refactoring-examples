package ExtractFunction;

import org.junit.Test;

public class TestPrintOwing {

    @Test
    public void NoVariableOutOfScopeBeforeRunner() {
        PrintOwing_before testClass = new PrintOwing_before();

        Invoice testInvoice = new Invoice("testCustomer");
        testInvoice.addOrder(23);
        testInvoice.addOrder(26);

        testClass.printOwing(testInvoice);
    }

    @Test
    public void NoVariableOutOfScopeAfterRunner() {
        PrintOwing_after testClass = new PrintOwing_after();

        Invoice testInvoice = new Invoice("testCustomer");
        testInvoice.addOrder(23);
        testInvoice.addOrder(26);

        testClass.printOwing(testInvoice);
    }

}
