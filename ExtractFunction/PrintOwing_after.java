package ExtractFunction;

import java.util.ArrayList;
import java.util.Calendar;

public class PrintOwing_after {

    public void printOwing(Invoice invoice) {

        printBanner();

        int outstanding = calculateOutstanding(invoice);

        recordDueDate(invoice);

        printDetails(invoice, outstanding);

    }

    private void printBanner() {
        System.out.println("************************");
        System.out.println("***** Customer Ows *****");
        System.out.println("************************");
    }

    private void printDetails(Invoice invoice, int outstanding) {
        System.out.println("name: " + invoice.getCustomer());
        System.out.println("amount: " + outstanding);
        System.out.println("due: " + invoice.getDueDate().getTime());
    }

    private void recordDueDate(Invoice invoice) {
        Calendar today = Calendar.getInstance();
        Calendar dueDate = today;
        dueDate.add(Calendar.DATE, 30);
        invoice.dueDate = dueDate;
    }

    private int calculateOutstanding(Invoice invoice) {
        int result = 0;
        for (Order o : invoice.getOrders()) {
            result += o.getAmount();
        }
        return result;
    }

}

class Invoice {
    String customer;
    ArrayList<Order> orders;
    Calendar dueDate;

    public Invoice(String customer) {
        this.customer = customer;
        dueDate = Calendar.getInstance();
        this.orders = new ArrayList<>();
    }

    public void addOrder(int amount) {
        Order order = new Order();
        order.setAmount(amount);
        orders.add(order);
    }

    public String getCustomer() {
        return this.customer;
    }

    public ArrayList<Order> getOrders() {
        return this.orders;
    }

    public Calendar getDueDate() {
        return this.dueDate;
    }
}

class Order {
    private static int maxId;
    private String id;
    private int amount;

    public Order() {
        int max = Order.maxId;
        max++;
        Order.maxId = max;
        this.id = Integer.toString(maxId);
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
